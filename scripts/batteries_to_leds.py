#!/usr/bin/env python

import rospy
import gd_motors_brd.msg
import std_msgs.msg

pub = None

minimumCapacity = 500 #(mAh)
def subs_cb(msg):
    if msg.battery[0].connected :
		if msg.battery[0].remaining_capacity < minimumCapacity :
			pub.publish('r')
		else :
			pub.publish('f')

if __name__ == '__main__':
    rospy.init_node('node_name')
    
    rospy.Subscriber('Batt_read', gd_motors_brd.msg.BatteryRegsArray,subs_cb)
	
    pub = rospy.Publisher('effects', std_msgs.msg.String, queue_size=1)
    
    rate = rospy.Rate(10) 
    while not rospy.is_shutdown():
    #while True:
        rate.sleep()
